<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Document</title>
</head>

<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/send" method="POST">
        @csrf
        <label>First name:</label>
        <br />
        <input type="text" name="namadepan" />
        <br />
        <br />
        <label>Last name:</label>
        <br />
        <input type="text" name="namabelakang" />
        <br />
        <br />
        <label>Gender:</label>
        <br />
        <input type="radio" name="gender" />Male
        <br />
        <input type="radio" name="gender" />Female
        <br />
        <input type="radio" name="gender" />Other
        <br />
        <br />
        <label>Nationality:</label>
        <br />
        <br />
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="foreign">Foreign</option>
        </select>
        <br />
        <br />
        <label>Language Spoken:</label>
        <br />
        <br />
        <input type="checkbox" />Bahasa Indonesia
        <br />
        <input type="checkbox" />English
        <br />
        <input type="checkbox" />Other
        <br />
        <br />
        <label>Bio:</label>
        <br />
        <br />
        <textarea name="bio" id="" cols="30" rows="10"></textarea>
        <br />
        <input type="submit" value="Sign Up" />
    </form>
</body>

</html>